<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220211120816 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE [user_account] (id INT IDENTITY NOT NULL, character NVARCHAR(32) NOT NULL, username NVARCHAR(80) NOT NULL, email_address NVARCHAR(80) NOT NULL, first_name NVARCHAR(80) NOT NULL, last_name NVARCHAR(80) NOT NULL, password NVARCHAR(98) NOT NULL, force_password_reset BIT NOT NULL, enabled BIT NOT NULL, last_login_date DATETIME2(6), last_login_ip NVARCHAR(45), PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_253B48AE937AB034 ON [user_account] (character) WHERE character IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_253B48AEF85E0677 ON [user_account] (username) WHERE username IS NOT NULL');
        $this->addSql('EXEC sp_addextendedproperty N\'MS_Description\', N\'(DC2Type:datetime_immutable)\', N\'SCHEMA\', \'dbo\', N\'TABLE\', \'[user_account]\', N\'COLUMN\', last_login_date');
        $this->addSql('ALTER TABLE [user_account] ADD CONSTRAINT DF_253B48AE_9991755D DEFAULT 0 FOR force_password_reset');
        $this->addSql('ALTER TABLE [user_account] ADD CONSTRAINT DF_253B48AE_50F9BB84 DEFAULT 1 FOR enabled');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('DROP TABLE [user_account]');
    }
}
