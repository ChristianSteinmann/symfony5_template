<?php

namespace App\Entity;

use App\Repository\UserAccountRepository;
use ASG\Bundle\ASGBundle\Entity\AbstractAccount;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;

#[ORM\Entity(repositoryClass: UserAccountRepository::class)]
#[ORM\Table(name: '`user_account`')]
class UserAccount extends AbstractAccount
{
    #[Column(type: "string", length: 32, unique: true, nullable: false)]
    private string $character;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getCharacter(): string
    {
        return $this->character;
    }

    /**
     * @param string $character
     */
    public function setCharacter(string $character): void
    {
        $this->character = $character;
    }
}
