<?php

declare(strict_types=1);

namespace App\Components;

use ASG\Bundle\ASGBundle\Components\FieldFormatter;
use ASG\Bundle\ASGBundle\Components\IFieldFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

define('FIELD_FORMATTER_ACCOUNT_ROLE_LIST', 256);
define('FIELD_FORMATTER_ACCOUNT_ROLE_PERMISSIONS_LIST', 257);
define('FIELD_FORMATTER_PUP_STATUS_DISPLAY', 258);

/**
 * Class SystemFormatter
 * @package App\Components
 */
class SystemFormatter implements IFieldFormatter
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * SystemFormatter constructor.
     * @param ContainerInterface $container
     * @param Security $security
     * @param Environment $twig
     */
    public function __construct(ContainerInterface $container, Security $security, Environment $twig)
    {
        $this->container = $container;
        $this->security = $security;
        $this->twig = $twig;
    }

    /**
     * @param $field
     * @param int $type
     * @param null $id
     * @param null $fieldName
     * @param FieldFormatter $fieldFormatter
     * @return mixed
     */
    function format($field, int $type, $id, $fieldName, FieldFormatter $fieldFormatter)
    {
        return $field;
    }
}
