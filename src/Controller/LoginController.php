<?php

declare(strict_types=1);

/**
 * @author Christian Steinmann <christian@asgsolutions.co.za>
 * @copyright Copyright (c), ASG Performance Solutions
 * Date: 2022/02/11
 * Time: 14:48
 */


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    #[Route('/login', name: 'login')]
    public function index(): Response
    {
        return $this->render('login/index.html.twig', [
            'controller_name' => 'LoginController',
        ]);
    }
}